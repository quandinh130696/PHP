<?php
require_once '/config/db.php';
class product {
    public $productID;
    public $productName;
    public $cateID;
    public $price;
    public $quantity;
    public $description;
    public $picture;
    public function __construct($pro_name,$cate_id,$price,$quantity,$desc,$picture)
    {
     
        $this -> productName = $pro_name;
        $this -> cateID = $cate_id;
        $this -> price = $price;
        $this -> quantity = $quantity;
        $this -> description = $desc;
        $this -> picture = $picture;
    }
    public function save()
    {
        $db = new Db();
        $sql = "INSERT INTO Product (ProductName,CateID,Price,Quantity,Description,Picture)
        VALUES ('$this->productName','$this->cateID','$this->price','$this->quantity','$this->description','$this->picture')   
        ";
        $result = $db ->query_excute($sql);
        return $result;
    }
     public function edit($id) {
        $db = new Db();
        $sql = "UPDATE product SET ProductName='$this->productName',CateID='$this->cateID',Price='$this->price',Quantity='$this->quantity',Description='$this->description',Picture='$this->picture' WHERE ProductID='$id'";
        $result = $db->query_excute($sql);
        return $result;
    }
    public static function list_product()
    {
        $db = new Db();
        $sql = "SELECT * from product";
        $result = $db ->select_to_array($sql);
        return   $result;
    }
     public static function list_product_by_cateId($cateid)
    {
        $db = new Db();
        $sql = "SELECT * from product WHERE CateID='$cateid'";
        $result = $db ->select_to_array($sql);
        return   $result;
    }
        public static function list_category()
    {
        $db = new Db();
        $sql = "SELECT * from category";
        $result = $db ->select_to_array($sql);
        return   $result;
    }
      public static function category_by_cateId($CateID)
    {
        $db = new Db();
        $sql = "SELECT * from category where CateID !='$CateID'";
        $result = $db ->select_to_array($sql);
        return   $result;
    }
     public function layid($id1) {
        $db = new Db();
        $sql = "select p.ProductName,p.Description, p.Picture, p.Price,p.Quantity ,c.CateID,c.CategoryName from product p
left join category c on c.CateID = p.CateID
WHERE  p.ProductID='$id1'";
        $result = $db->select_to_array($sql);
        return $result;
    }
      public function delete($idpro) {
        $db = new Db();
        mysql_query("SET NAMES 'UTF8'");
        $sql = "delete from product where ProductID = '$idpro' ";
        $result = $db->query_excute($sql);
        return $result;
    }
}




?>