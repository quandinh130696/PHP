-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 05, 2018 at 12:15 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `banqao`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` bigint(20) NOT NULL,
  `ky_danh` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `ky_danh`, `password`) VALUES
(1, 'quan', '123');

-- --------------------------------------------------------

--
-- Table structure for table `bill`
--

CREATE TABLE `bill` (
  `bill_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `address` text CHARACTER SET utf8 NOT NULL,
  `date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `total` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bill`
--

INSERT INTO `bill` (`bill_id`, `user_id`, `address`, `date`, `total`) VALUES
(1, 1, 'Ha Noi', '2018-03-27 17:00:00', 560),
(2, 1, 'Ha Noi', '2018-03-29 17:00:00', 9000),
(3, 1, 'Ha Noi', '2018-03-30 17:00:00', 500);

-- --------------------------------------------------------

--
-- Table structure for table `bill_detail`
--

CREATE TABLE `bill_detail` (
  `bill_detail_id` bigint(20) NOT NULL,
  `bill_id` bigint(20) NOT NULL,
  `ma_sanpham` bigint(20) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bill_detail`
--

INSERT INTO `bill_detail` (`bill_detail_id`, `bill_id`, `ma_sanpham`, `quantity`, `price`) VALUES
(1, 1, 16, 0, 560),
(2, 2, 1, 1, 9000),
(3, 3, 2, 3, 100),
(4, 3, 3, 1, 200);

-- --------------------------------------------------------

--
-- Table structure for table `hangthoitrang`
--

CREATE TABLE `hangthoitrang` (
  `ma_hang` bigint(20) NOT NULL,
  `ten_hang` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hangthoitrang`
--

INSERT INTO `hangthoitrang` (`ma_hang`, `ten_hang`) VALUES
(2, 'ARMANI'),
(3, 'Valentino'),
(4, 'pierre');

-- --------------------------------------------------------

--
-- Table structure for table `kichco`
--

CREATE TABLE `kichco` (
  `ma_kichco` bigint(20) NOT NULL,
  `ten_kichco` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kichco`
--

INSERT INTO `kichco` (`ma_kichco`, `ten_kichco`) VALUES
(1, 'S'),
(2, 'M'),
(3, 'L'),
(4, 'XL');

-- --------------------------------------------------------

--
-- Table structure for table `loaithoitrang`
--

CREATE TABLE `loaithoitrang` (
  `ma_loai` bigint(20) NOT NULL,
  `ten_loai` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loaithoitrang`
--

INSERT INTO `loaithoitrang` (`ma_loai`, `ten_loai`) VALUES
(1, 'Nam'),
(2, 'Nữ');

-- --------------------------------------------------------

--
-- Table structure for table `sanpham`
--

CREATE TABLE `sanpham` (
  `ma_sanpham` bigint(20) NOT NULL,
  `ten_sanpham` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `hinh_anh` varchar(200) CHARACTER SET utf8 NOT NULL,
  `gia` decimal(9,3) NOT NULL,
  `mo_ta` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ma_loai` bigint(20) NOT NULL,
  `ma_hang` bigint(20) NOT NULL,
  `ma_kichco` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sanpham`
--

INSERT INTO `sanpham` (`ma_sanpham`, `ten_sanpham`, `hinh_anh`, `gia`, `mo_ta`, `ma_loai`, `ma_hang`, `ma_kichco`) VALUES
(1, 'Sơ mi công sở', 'sp.jpg', '9000.000', 'Sơ mi công sở', 1, 2, 2),
(2, 'Áo khoác Bombe', 'ao-khoac-bomber-nam-den-in-chu-h01_1_den_master.jpg', '100.000', 'Phong cách trẻ trung , dễ phối đồ .', 1, 4, 3),
(3, 'Đầm hoa', 'cachphoidodichoichonu11.jpg', '200.000', '', 2, 2, 2),
(4, 'Váy ngắn', 'ao-so-mi-nu-dinh-no-cach-dieu-12-600x736.jpg', '600.000', '', 2, 4, 3),
(5, 'Áo thun blue', 'product12.jpg', '200.000', 'Chất liệu mát mẻ , dễ mặc', 1, 4, 3),
(6, 'Áo khoác dài nữ', 'mau-ao-khoac-da-qua-goi-dep-625x841.png', '900.000', '', 2, 4, 2),
(11, 'Đầm phương Tây huyền bí', 'BST-phuong-Dong-huyen-bi-cua-Armani-6.jpg', '200.000', 'Là một trong những sản phẩm đậm chất quý phái của bộ sưu tập ARMANI ', 2, 2, 2),
(12, 'Set xám', 'thoitrang.jpg', '500.000', '', 2, 4, 1),
(13, 'Vest nữ ARMANI', 'bo-suu-tap-giorgio-armani-thu-dong-2016-rtw-13-800x1422.jpg', '2000.000', '', 2, 2, 1),
(14, 'Tây phục nam ARMANI', 'giacca-doppiopetto-giorgio-armani.jpg', '600.000', 'Còn nhiều kích cỡ S, M , L , XL', 1, 2, 4),
(15, 'Áo khoác ARMANI', 'Amani.jpg', '2000.000', '', 2, 2, 2),
(16, 'ARMANI Xuân hè', 'BST-Emporio-Armani-Xuân-Hè-2016-1-490x735.jpg', '560.000', '', 2, 2, 1),
(17, 'moi', 'thoi-trang-cong-so-nu.png', '200.000', '444', 1, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` bigint(20) NOT NULL,
  `user_email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_fullname` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_pass` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_email`, `user_fullname`, `user_pass`) VALUES
(1, 'quan.dinh130696@gmail.com', 'quandinh', '123'),
(2, '', '', ''),
(3, 'hyy', 'huyha.hutech1996@gmail.com', '123'),
(4, 'hyy', 'huyha.hutech1996@gmail.com', '123'),
(5, 'hyy', 'huyha.hutech1996@gmail.com', '123'),
(6, 'quzn', 'q@gmail.com', '123'),
(7, '', 'quan@123.com', 'Quan123'),
(8, '', 'quan@123.com', 'Quan123'),
(9, '', 'quan@678', 'Quan123'),
(10, 'quan@678', '', 'Quan123'),
(11, 'quan@1234', '', 'Quan123'),
(12, 'quan@12123', '', 'Quan123'),
(13, 'k@123', '', '123'),
(14, 'k@123', '', '123'),
(15, 'quan@gmail.com', 'Dinh Quan Nguyen', '456'),
(16, 'quan123@yahoo.cpm', 'Quan', '123'),
(17, 'quan123@yahoo.cpm', 'Quan', '123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`bill_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `bill_detail`
--
ALTER TABLE `bill_detail`
  ADD PRIMARY KEY (`bill_detail_id`),
  ADD KEY `ma_sanpham` (`ma_sanpham`),
  ADD KEY `bill_id` (`bill_id`);

--
-- Indexes for table `hangthoitrang`
--
ALTER TABLE `hangthoitrang`
  ADD PRIMARY KEY (`ma_hang`);

--
-- Indexes for table `kichco`
--
ALTER TABLE `kichco`
  ADD PRIMARY KEY (`ma_kichco`);

--
-- Indexes for table `loaithoitrang`
--
ALTER TABLE `loaithoitrang`
  ADD PRIMARY KEY (`ma_loai`);

--
-- Indexes for table `sanpham`
--
ALTER TABLE `sanpham`
  ADD PRIMARY KEY (`ma_sanpham`),
  ADD KEY `ma_hang` (`ma_hang`),
  ADD KEY `ma_loai` (`ma_loai`),
  ADD KEY `ma_kichco` (`ma_kichco`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bill`
--
ALTER TABLE `bill`
  MODIFY `bill_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bill_detail`
--
ALTER TABLE `bill_detail`
  MODIFY `bill_detail_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `hangthoitrang`
--
ALTER TABLE `hangthoitrang`
  MODIFY `ma_hang` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kichco`
--
ALTER TABLE `kichco`
  MODIFY `ma_kichco` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `loaithoitrang`
--
ALTER TABLE `loaithoitrang`
  MODIFY `ma_loai` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sanpham`
--
ALTER TABLE `sanpham`
  MODIFY `ma_sanpham` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bill`
--
ALTER TABLE `bill`
  ADD CONSTRAINT `bill_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `bill_detail`
--
ALTER TABLE `bill_detail`
  ADD CONSTRAINT `bill_detail_ibfk_1` FOREIGN KEY (`ma_sanpham`) REFERENCES `sanpham` (`ma_sanpham`),
  ADD CONSTRAINT `bill_detail_ibfk_2` FOREIGN KEY (`bill_id`) REFERENCES `bill` (`bill_id`);

--
-- Constraints for table `sanpham`
--
ALTER TABLE `sanpham`
  ADD CONSTRAINT `sanpham_ibfk_1` FOREIGN KEY (`ma_hang`) REFERENCES `hangthoitrang` (`ma_hang`),
  ADD CONSTRAINT `sanpham_ibfk_2` FOREIGN KEY (`ma_loai`) REFERENCES `loaithoitrang` (`ma_loai`),
  ADD CONSTRAINT `sanpham_ibfk_3` FOREIGN KEY (`ma_kichco`) REFERENCES `kichco` (`ma_kichco`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
