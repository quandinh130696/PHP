<?php ob_start(); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Quan Shop</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/prettyPhoto.css" rel="stylesheet">
        <link href="css/price-range.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    </head><!--/head-->

    <body>

        <?php
        session_start();
        include 'header.php';
        ?>

        <section id="cart_items">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="#">Trang chủ</a></li>
                        <li class="active">Giỏ hàng </li>
                    </ol>
                </div>
                <p><?php
              
                    $ok = 1;
                    if (isset($_SESSION['cart'])) {
                        foreach ($_SESSION['cart'] as $key => $value) {
                            if (isset($key)) {
                                $ok = 2;
                            }
                        }
                    }

                   
                    

                    // $sl = count($_SESSION['cart']);
                    ?>
                    <?php
                    if (count($_SESSION['cart']) == 0) {
                        header('Location: baogiohangtrong.php');
                    }
                    ?>
                </p>			
                <div class="table-responsive col-sm-9 cart_info padding-right">
                    <table class="table table-condensed">
                        <thead>
                            <tr class="cart_menu">
                                <td class="image">Sản phẩm</td>
                                <td class="description"></td>
                                <td class="price">Giá</td>
                                <td class="quantity">Số lượng</td>
                                <td class="total">Tổng cộng</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
<?php
require "inc/myconnect.php";

if (isset($_SESSION['cart'])) {
    foreach ($_SESSION['cart'] as $key => $value) {
        $item[] = $key;
    }
    // echo $item;
    $str = implode(",", $item);
    $query = "SELECT s.ma_sanpham,s.ten_sanpham,s.hinh_anh,s.gia,s.mo_ta,h.ten_hang 
				as tenhang,c.ten_kichco as tenkichco ,
				 l.ten_loai as tenloai from sanpham s 
				 LEFT JOIN hangthoitrang h on h.ma_hang = s.ma_hang 
				 LEFT JOIN kichco c on c.ma_kichco = s.ma_kichco 
				 LEFT JOIN loaithoitrang l on l.ma_loai = s.ma_loai 
				 WHERE s.ma_sanpham  in ($str)";
    $result = $conn->query($query);
    $total = "";
    foreach ($result as $s) {
        ?>
                                    <?php
                                    if (isset($_POST['remove'])) {
                                        $idsprm = $_POST["idsprm"];
                                        $sl;
                                        if (isset($_SESSION['cart'][$idsprm])) {
                                            unset($_SESSION['cart'][$idsprm]);
                                            header('Location: cart.php');
                                        }
                                    }
                                    if (isset($_POST['update'])) {
                                        session_start();
                                        $sl = $_POST["qty"];
                                        foreach ($sl as $key => $value) {
                                            if (($value == 0) and ( is_numeric($value))) {
                                                unset($_SESSION['cart'][$key]);
                                            }
                                            if (($value > 0) and ( is_numeric($value))) {
                                                $_SESSION['cart'][$key] = $value;
                                            }
                                        }
                                        header('Location: cart.php');
                                    }
                                    ?>		

                                    <tr>
                                <form name="form4" id="ff4" method="POST" action="#">
                                    <td class="cart_product">
                                        <a href=""><img src="images/shop/<?php echo $s["hinh_anh"] ?>" style="width:80px" alt=""></a>
                                    </td>
                                    <td class="cart_description">
                                        <h4><a href=""><?php echo $s["ten_sanpham"] ?></a></h4>
                                        <p>Web ID: <?php echo $s["ma_sanpham"] ?></p>
                                    </td>
                                    <td class="cart_price">
                                        <p><?php echo $s["gia"] ?></p>
                                    </td>
                                    <td class="cart_quantity">
                                        <div class="cart_quantity_button">
                                            <input class="form-inline quantity"  min="1" max ="99" type="number" name ="qty[<?php echo $s["ma_sanpham"] ?>]" value="<?php echo $_SESSION['cart'][$s["ma_sanpham"]] ?>"> 
                                            <input type="submit" name="update"  value="cập nhật sản phẩm này" class="btn btn-2" />
                                        </div>
                                    </td>
                                    <td class="cart_total">
                                        <p class="cart_total_price"><?php echo $_SESSION['cart'][$s["ma_sanpham"]] * $s["gia"] ?>.000</p>
                                    </td>
                                    <td class="cart_delete">
                                        <input type="hidden" name="idsprm" value="<?php echo $s["ma_sanpham"] ?>" />
                                        <input type="submit" name="remove" value="xóa sản phẩm này" class="btn btn-2"  />
                                    </td>
                                </form>
                                </tr>
        <?php $total += $_SESSION['cart'][$s["ma_sanpham"]] * $s["gia"] ?>
        <?php
    }
}
?>
                        </tbody>
                    </table>
                    <h2>Thành tiền :<strong style="color:red"> <?php echo $total ?>.000<strong></h2><a href="rmcart.php" class="btn btn-2">Xóa hết giỏ hàng</a>
                                <form name="form5" id="ff5" method="POST" action="#">
                                    <input type="submit" name="dathang" value="Đặt hàng sản phẩm" class="btn btn-2"  />
                                </form>

                                </div>
<?php
if (isset($_POST['dathang'])) {

    header('Location: dathang.php');
}
?>

                                </div>

                                </section> <!--/#cart_items-->




<?php
include 'footer.php';
?>

                                <script src="js/jquery.js"></script>
                                <script src="js/bootstrap.min.js"></script>
                                <script src="js/jquery.scrollUp.min.js"></script>
                                <script src="js/jquery.prettyPhoto.js"></script>
                                <script src="js/main.js"></script>
                                </body>
                                </html>
								
								
<?php ob_end_flush () ?>