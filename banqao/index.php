<?php  session_start(); ?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Home | Quan-Shop's</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/prettyPhoto.css" rel="stylesheet">
        <link href="css/price-range.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    </head>
    <body>
        <?php
        include 'header.php';
 
        ?>
        <section>
           <div class="container">
			<div class="row">
			<div>
            <?php
                        	
if(isset($_POST['submit']))
{
    $idsp = $_POST["idsp"];
    $sl;
        if(isset($_SESSION['cart'][$idsp]))
        {
            $sl = $_SESSION['cart'][$idsp] +1;
        }
        else
        {
            $sl=1;
        }
        $_SESSION['cart'][$idsp] = $sl;

}
?>
            </div>	 
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Các sản phẩm mới</h2>
                                                <?php 
                                                require 'inc/truyvan.php';
                                                if ($sptc->num_rows > 0) {
    // output data of each row
    while($row = $sptc->fetch_assoc()) {

                                                ?>
                                                <div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<div class="productinfo text-center">
											<img style="width: 180px; height: 250px" src="images/shop/<?php echo $row['hinh_anh']?>" alt="" />
											<h2><?php echo $row['gia']?></h2>
											<p><?php echo $row['ten_sanpham']?></p>
                                            <form name="form3" id="ff3" method="POST" action="#">
     
         <input type="hidden" name="acction" value="them vao gio hang" />
		 <input type="hidden" name="idsp" value="<?php echo $row["ma_sanpham"] ?>" />
         </form>
										</div>
									
								</div>
								<div class="choose">
									<ul class="nav nav-pills nav-justified">
										
                                                                            <li><a href="product-details.php?id=<?php echo $row["ma_sanpham"]?>"><i class="fa fa-plus-square"></i>Xem chi tiết</a></li>
									</ul>
								</div>
							</div>
						</div>
		<?php
                }
                }
                ?>
					</div><!--features_items-->
					
					
					
					
					
				</div>
			</div>
		</div>
        </section>
        <?php
        include 'footer.php';
        ?>

        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.scrollUp.min.js"></script>
        <script src="js/price-range.js"></script>
        <script src="js/jquery.prettyPhoto.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>