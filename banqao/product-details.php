<?php  session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Chi tiết sản phẩm | Quan - Shop</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
    
	<?php
        include 'header.php';
        ?>
	
	
	<section>
		<div class="container">
			<div class="row">
				
		<?php
                	
   require 'inc/myconnect.php';
   
   //lay san pham theo id
   $id = $_GET["id"];
   $query="SELECT s.ma_sanpham,s.ten_sanpham,s.hinh_anh,s.gia,s.mo_ta,h.ten_hang as tenhang,c.ten_kichco as tenkichco , l.ten_loai as tenloai from sanpham s LEFT JOIN hangthoitrang h on h.ma_hang = s.ma_hang LEFT JOIN kichco c on c.ma_kichco = s.ma_kichco LEFT JOIN loaithoitrang l on l.ma_loai = s.ma_loai WHERE s.ma_sanpham =".$id;
   $result = $conn->query($query);
    $row = $result->fetch_assoc();

?>	
		<div></div><?php
if(isset($_POST['submit']))
{
    $idsp = $_POST["idsp"];
    $sl;
        if(isset($_SESSION['cart'][$idsp]))
        {
            $sl = $_SESSION['cart'][$idsp] +1;
        }
        else
        {
            $sl=1;
        }
        $_SESSION['cart'][$idsp] = $sl;

}
?>			
				<div class="col-sm-9 padding-right">
					<div class="product-details"><!--product-details-->
                                            <h2 class="title text-center">Chi tiết sản phẩm</h2>
						<div class="col-sm-5">
							<div class="view-product">
								<img src="images/shop/<?php echo $row["hinh_anh"]?>" alt="" />
								
							</div>
							

						</div>
						<div class="col-sm-7">
							<div class="product-information"><!--/product-information-->
								<img src="images/product-details/new.jpg" class="newarrival" alt="" />
								<h2><?php echo $row["ten_sanpham"]?></h2>
                                                                <p>Thương hiệu :<?php echo $row["tenhang"]?></p>
                                                                 <p>Thời trang phái :<?php echo $row["tenloai"]?></p>
								 <p>Kích cỡ :<?php echo $row["tenkichco"]?></p>
								
								<span>
									<span><?php echo $row["gia"]?> VND</span>								
                                                                  
								</span>
                                                                <p>Mô tả :<?php echo $row["mo_ta"]?></p>
                                                                      <div>
                                                                      <form name="form3" id="ff3" method="POST" action="#">
                                                                      <!-- <input type="submit" name="submit" class="btn btn-2" value="Đặt hàng" /> -->
									<button type="submit" class="btn btn-fefault cart" name="submit">                                
										<i class="fa fa-shopping-cart"></i>
										Thêm vào giỏ hàng
									</button>
                                                                        <input type="hidden" name="acction" value="them vao gio hang" />
								        <input type="hidden" name="idsp" value="<?php echo $row["ma_sanpham"] ?>" />
                                                                        </form>
                                                                          </div>
							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->
					
					
					
					
					
				</div>
			</div>
		</div>
	</section>
	
	<?php
        include 'footer.php';
        
        ?>

  
   
</body>
</html>