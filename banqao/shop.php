<?php
       session_start();
 
        ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sản phẩm | Quan - Shop</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    </head>
    <body>
        <?php
        include 'header.php';
        
        ?>
        
       <section>
		<div class="container">
			<div class="row">
            <div>
            <?php
                        	
if(isset($_POST['submit']))
{
    $idsp = $_POST["idsp"];
    $sl;
        if(isset($_SESSION['cart'][$idsp]))
        {
            $sl = $_SESSION['cart'][$idsp] +1;
        }
        else
        {
            $sl=1;
        }
        $_SESSION['cart'][$idsp] = $sl;

}
?>
            </div>
        		<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Sản phẩm theo hãng thời trang</h2>
                                                            <?php 
                                                require 'inc/truyvan.php';
                                                //lay danh sach thoi trang
                                                $mahang = $_GET['mahang'];
                                                $result = mysqli_query($conn, "select count(ma_sanpham) as total from sanpham where ma_hang='$mahang' ");
                                                $row = mysqli_fetch_assoc($result);
                                                $total_records = $row['total'];
                                                // BƯỚC 3: TÌM LIMIT VÀ CURRENT_PAGE
                                                $current_page = isset($_GET['page']) ? $_GET['page'] : 1;
                                                $limit = 6;
                                                // BƯỚC 4: TÍNH TOÁN TOTAL_PAGE VÀ START
                                                // tổng số trang
                                                $total_page = ceil($total_records / $limit);
                                                 
                                                // Giới hạn current_page trong khoảng 1 đến total_page
                                                if ($current_page > $total_page){
                                                    $current_page = $total_page;
                                                }
                                                else if ($current_page < 1){
                                                    $current_page = 1;
                                                }
                                                 
                                                // Tìm Start
                                                $start = ($current_page - 1) * $limit;
                                                 
                                                // BƯỚC 5: TRUY VẤN LẤY DANH SÁCH TIN TỨC
                                                // Có limit và start rồi thì truy vấn CSDL lấy danh sách tin tức
                                                $result = mysqli_query($conn, "SELECT * FROM sanpham where ma_hang='$mahang' LIMIT $start, $limit " );
                                                while ($row = mysqli_fetch_assoc($result)){


                                                ?>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<img style="width: 180px; height: 250px" src="images/shop/<?php echo $row['hinh_anh']?>" alt="" />
										<h2><?php echo $row['gia']?></h2>
										<p><?php echo $row['ten_sanpham']?></p>
                                        <form name="form3" id="ff3" method="POST" action="#">
         <button type="submit"  class="btn btn-default add-to-cart" name="submit">                                
										<i class="fa fa-shopping-cart"></i>
										Thêm vào giỏ hàng
									</button>
         <input type="hidden" name="acction" value="them vao gio hang" />
		 <input type="hidden" name="idsp" value="<?php echo $row["ma_sanpham"] ?>" />
         </form>
									</div>
									
								</div>
								<div class="choose">
									<ul class="nav nav-pills nav-justified">
										
                                                                            <li><a href="product-details.php?id=<?php echo $row["ma_sanpham"] ?>"><i class="fa fa-plus-square"></i>Xem chi tiết</a></li>
									</ul>
								</div>
							</div>
						</div>
			<?php
                        }
                        ?>
						

                                        </div>
                      </div>
                      </div>
			</div>
            <div class="row text-center">
           <?php 
            // PHẦN HIỂN THỊ PHÂN TRANG
// BƯỚC 7: HIỂN THỊ PHÂN TRANG
 
// nếu current_page > 1 và total_page > 1 mới hiển thị nút prev
if ($current_page > 1 && $total_page > 1){
    echo '<a href="shop.php?mahang='.$mahang.'&page='.($current_page-1).'">Prev</a> | ';
}
 
// Lặp khoảng giữa
for ($i = 1; $i <= $total_page; $i++){
    // Nếu là trang hiện tại thì hiển thị thẻ span
    // ngược lại hiển thị thẻ a
    if ($i == $current_page){
        echo '<span>'.$i.'</span> | ';
    }
    else{
        echo '<a href="shop.php?mahang='.$mahang.'&page='.$i.'">'.$i.'</a> | ';
    }
}
 
// nếu current_page < $total_page và total_page > 1 mới hiển thị nút prev
if ($current_page < $total_page && $total_page > 1){
    echo '<a href="shop.php?mahang='.$mahang.'&page='.($current_page+1).'">Next</a> | ';
}
           ?>
        </div>
       </section>
                                       
        <?php
        include 'footer.php';
       
        ?>
    </body>
</html>
<script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.scrollUp.min.js"></script>
        <script src="js/price-range.js"></script>
        <script src="js/jquery.prettyPhoto.js"></script>
        <script src="js/main.js"></script>