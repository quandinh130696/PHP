<?php ob_start();?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Cập nhật sản phẩm</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    </head>
   <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
        <?php
        include 'header.php';
        ?>

        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Cập nhật sản phẩm

                </h1>

            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">

                            </div><!-- /.box-header -->
                            <!-- form start -->
                            <form role="form" method="POST" action="#">
                            <?php
   require '../inc/myconnect.php';
   //lay san pham theo id
   $id = $_GET["masp"];
   $query="SELECT s.ma_sanpham,s.ten_sanpham,s.hinh_anh,s.gia,s.mo_ta,h.ten_hang as
    tenhang,c.ten_kichco as tenkichco ,l.ten_loai as tenloai,h.ma_hang,l.ma_loai,c.ma_kichco from sanpham s 
    LEFT JOIN hangthoitrang h on h.ma_hang = s.ma_hang 
    LEFT JOIN kichco c on c.ma_kichco = s.ma_kichco 
    LEFT JOIN loaithoitrang l on l.ma_loai = s.ma_loai WHERE s.ma_sanpham ="  .$id;

   $result = $conn->query($query);
$row = $result->fetch_assoc();

?>
<?php 
    if(isset($_POST['Edit']))
    {
    require '../inc/myconnect.php';
    $id = $_POST['id'];
    $name = $_POST['name'];
    $hangthoirang = $_POST['hangthoirang'];
    $thoitrangphai = $_POST['thoitrangphai'];
    $kichco  = $_POST['kichco'];
    $gia = $_POST['gia'];
    $mota = $_POST['mota'];
    $hinhanh = $_POST['hinhanh'];
        $sql = "UPDATE sanpham SET ten_sanpham='$name', hinh_anh='$hinhanh', ma_hang= '$hangthoirang', ma_kichco='$kichco ',gia='$gia', 
        ma_loai='$thoitrangphai', mo_ta='$mota'
        WHERE ma_sanpham= '$id ' " ;
        if ($conn->query($sql) === TRUE) {
            header('Location: qlysanpham.php');
        } else {
            echo "Error updating record: " . $conn->error;
        }
        $conn->close();  
    }
?>

                            <div class="box-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Mã sản phẩm - tự tăng:</label>
                                        <input type="type" name="id" class="form-control"  readonly="" value="<?php  echo $row["ma_sanpham"] ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Tên sản phẩm:</label>
                                        <input type="type" class="form-control" value="<?php  echo $row["ten_sanpham"] ?>" name="name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Giá tiền:</label>
                                        <input type="text" name="gia" value ="<?php echo $row["gia"] ?>" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Mô tả:</label>
                                        <input type="text" name="mota" value="<?php echo $row["mo_ta"] ?>" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Kích cỡ:</label>
                                        <select name="kichco">
                                        <option selected="selected" value="<?php echo $row["ma_kichco"] ?>"><?php echo $row["tenkichco"] ?></option>
                                        <?php
                         require '../inc/myconnect.php';
                         $sql="SELECT * from kichco where ma_kichco !=".$row["ma_kichco"];
                         $result = $conn->query($sql); 
                         if ($result->num_rows > 0) {
                          // output data of each row
                          while($rows = $result->fetch_assoc()) {
                      ?>
                      <option value="<?php echo $rows["ma_kichco"] ?>"><?php echo $rows["ten_kichco"] ?></option>
                      <?php 
                          }
                        }
                      ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Thời trang phái:</label>
                                        <select name="thoitrangphai"> 
                                        <option value="<?php echo $row["ma_loai"] ?>"><?php echo $row["tenloai"] ?></option>                                     
                                        <?php
                         require '../inc/myconnect.php';
                         $sql="SELECT * from loaithoitrang where ma_loai !=".$row["ma_loai"];
                         $result = $conn->query($sql); 
                         if ($result->num_rows > 0) {
                          // output data of each row
                          while($rowsss = $result->fetch_assoc()) {
                      ?>
                      <option value="<?php echo $rowsss["ma_loai"] ?>"><?php echo $rowsss["ten_loai"] ?></option>
                      <?php 
                          }
                        }
                      ?>                     
                           </select>
                                    </div>                                                
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Hãng thời trang:</label>   
                                        <select  name="hangthoirang">  
                    <option value="<?php echo $row["ma_hang"] ?>"><?php echo $row["tenhang"] ?></option>                                
                     <?php
                         require '../inc/myconnect.php';
                         $sql="SELECT * from hangthoitrang where ma_hang != ".$row["ma_hang"];
                         $result = $conn->query($sql); 
                         if ($result->num_rows > 0) {
                          // output data of each row
                          while($rowss = $result->fetch_assoc()) {
                      ?>
                      <option value="<?php echo $rowss["ma_hang"] ?>"><?php echo $rowss["ten_hang"] ?></option>
                      <?php 
                          }
                        }
                      ?>
                    </select>
                                    </div>                                           
                                    <div class="form-group">
                                        <label for="exampleInputFile">Hình ảnh</label>
                                        <input type="file" id="exampleInputFile"  value="<?php echo $row["hinh_anh"] ?>" name="hinhanh" required>                                   
                                    </div>                        
                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    <button type="submit" name="Edit" class="btn btn-primary">Sửa</button>                                    
                                       <a href="qlysanpham.php"> 
                                          <button type="button" class="btn btn-danger">Quay lại </button>                                         
                                    </a>                                   
                                </div>
                            </form>
                        </div><!-- /.box -->
                        <!-- Input addon -->
                    </div><!--/.col (left) -->
                    <!-- right column -->
                </div>   <!-- /.row -->
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <?php
        include 'footer.php';
        ?>
        </div>
    </body>
</html>
