<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Trang đăng nhập</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark">
    <?php
	//Gọi file connection.php ở bài trước
//	require_once("lib/connection.php");
  // Kiểm tra nếu người dùng đã ân nút đăng nhập thì mới xử lý
    $kq="";
	if (isset($_POST["btn_submit"])) {
    // lấy thông tin người dùng
    require '../inc/myconnect.php';
		$username = $_POST["username"];
		$password = $_POST["password"];
		//làm sạch thông tin, xóa bỏ các tag html, ký tự đặc biệt 
		//mà người dùng cố tình thêm vào để tấn công theo phương thức sql injection
		$username = strip_tags($username);
		$username = addslashes($username);
		$password = strip_tags($password);
		$password = addslashes($password);
		
			$sql = "select * from admin where ky_danh = '$username' and password = '$password' ";
			$query = mysqli_query($conn,$sql);
			$num_rows = mysqli_num_rows($query);
			if ($num_rows==0) {
        $kq = "tên đăng nhập hoặc mật khẩu không đúng !";
			}else{
				//tiến hành lưu tên đăng nhập vào session để tiện xử lý sau này
				$_SESSION['username'] = $username;
               // Thực thi hành động sau khi lưu thông tin vào session
               // ở đây mình tiến hành chuyển hướng trang web tới một trang gọi là index.php
               header('Location: index.php');
			}
              
	}
?>
    
  <div  class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Đăng nhập trang quản trị</div>
      <div class="card-body">
        <form method="POST" action="#">
            <fieldset>
          <div class="form-group">
            <label for="username">Username</label>
            <input required="yes" class="form-control" name="username" type="text" value="quan">
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input required="yes" class="form-control" name="password" type="password" value="123">
          </div>
         
             <input name="btn_submit" type="submit" class="btn btn-primary btn-block"value="Đăng nhập">
             <?php echo $kq?>
            </fieldset>
        </form>
       
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
