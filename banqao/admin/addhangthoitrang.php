<?php ob_start(); ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Thêm hãng thời trang</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

        <?php
        include 'header.php';
        ?>
            <?php
if(isset($_POST['create']))
{
    require '../inc/myconnect.php';
    $name = $_POST['name'];
    $sql="INSERT INTO  hangthoitrang (ten_hang) 
    VALUES ('$name') ";
    // echo  $mk;
    if (mysqli_query($conn, $sql)) {
        header('Location: qlyhangthoitrang.php');
    } else {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

 ?>
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Thêm mới hãng

                </h1>

            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">

                            </div><!-- /.box-header -->
                            <!-- form start -->
                            <form role="form" method="POST" action="#">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Mã hãng - tự tăng:</label>
                                        <input type="type" class="form-control" id="masp" readonly="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Tên hãng:</label>
                                        <input type="type" class="form-control" id="tensp" name="name" required>
                                    </div>                              
                                </div><!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" name="create" class="btn btn-primary">Thêm</button>          
                                       <a href="qlyhangthoitrang.php">
                                          <button type="button" class="btn btn-danger">Quay lại </button> 
                                    </a>
                                </div>
                            </form>
                        </div><!-- /.box -->





                        <!-- Input addon -->


                    </div><!--/.col (left) -->
                    <!-- right column -->

                </div>   <!-- /.row -->
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <?php
        include 'footer.php';
        ?>
        </div>
    </body>
</html>
