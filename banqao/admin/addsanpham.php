<?php ob_start(); ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
  
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Thêm sản phẩm</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    </head>
     <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
        <?php
        include 'header.php';
        ?>

        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Thêm mới sản phẩm

                </h1>

            </section>
            <?php
if(isset($_POST['create']))
{
    require '../inc/myconnect.php';
    $name = $_POST['name'];
    $hangthoirang = $_POST['hangthoirang'];
    $thoitrangphai = $_POST['thoitrangphai'];
    $kichco  = $_POST['kichco'];
    $gia = $_POST['gia'];
    $mota = $_POST['mota'];
    $hinhanh = $_POST['hinhanh'];
    $sql="INSERT INTO  sanpham (ten_sanpham,ma_hang,ma_loai,ma_kichco,gia,mo_ta,hinh_anh) 
    VALUES ('$name','$hangthoirang' ,'$thoitrangphai','$kichco',' $gia','$mota','$hinhanh') ";
    // echo  $mk;
    if (mysqli_query($conn, $sql)) {
        header('Location: qlysanpham.php');
    } else {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

 ?>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">

                            </div><!-- /.box-header -->
                            <!-- form start -->
                            <form role="form" method="POST" action="#">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Mã sản phẩm - tự tăng:</label>
                                        <input type="type" class="form-control"  readonly="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Tên sản phẩm:</label>
                                        <input type="type" class="form-control" name="name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Hãng thời trang:</label>   
                                        <select  name="hangthoirang">
                     <?php
                         require '../inc/myconnect.php';
                         $sql="SELECT * from hangthoitrang";
                         $result = $conn->query($sql); 
                         if ($result->num_rows > 0) {
                          // output data of each row
                          while($row = $result->fetch_assoc()) {
                      ?>
                      <option value="<?php echo $row["ma_hang"] ?>"><?php echo $row["ten_hang"] ?></option>
                      <?php 
                          }
                        }
                      ?>
                    </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Thời trang phái:</label>
                                        <select name="thoitrangphai">
                                        <?php
                         require '../inc/myconnect.php';
                         $sql="SELECT * from loaithoitrang";
                         $result = $conn->query($sql); 
                         if ($result->num_rows > 0) {
                          // output data of each row
                          while($row = $result->fetch_assoc()) {
                      ?>
                      <option value="<?php echo $row["ma_loai"] ?>"><?php echo $row["ten_loai"] ?></option>
                      <?php 
                          }
                        }
                      ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Kích cỡ:</label>
                                        <select name="kichco">
                                        <?php
                         require '../inc/myconnect.php';
                         $sql="SELECT * from kichco";
                         $result = $conn->query($sql); 
                         if ($result->num_rows > 0) {
                          // output data of each row
                          while($row = $result->fetch_assoc()) {
                      ?>
                      <option value="<?php echo $row["ma_kichco"] ?>"><?php echo $row["ten_kichco"] ?></option>
                      <?php 
                          }
                        }
                      ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Giá tiền:</label>
                                        <input type="text" name="gia" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Mô tả:</label>
                                        <input type="text" name="mota" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputFile">Hình ảnh</label>
                                        <input type="file" id="exampleInputFile" name="hinhanh">                                
                                    </div>

                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    <button type="submit" name="create" class="btn btn-primary">Thêm</button>
                                    <a href="qlysanpham.php"> 
                                          <button type="button" class="btn btn-danger">Quay lại </button>     
                                    </a>                                                                  
                                </div>
                            </form>
                        </div><!-- /.box -->





                        <!-- Input addon -->


                    </div><!--/.col (left) -->
                    <!-- right column -->

                </div>   <!-- /.row -->
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <?php
        include 'footer.php';
        ?>
        </div>
    </body>
</html>
