<?php ob_start(); ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Quản lý hãng thời trang</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    </head>
     <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
        <?php
        include 'header.php';
        ?>
         <?php include '../inc/myconnect.php';?>
             <?php include '../inc/function.php';?>
        <div class="content-wrapper">
       <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Quản lý hãng thời trang</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <a href="addhangthoitrang.php" 
                                    <button type="submit" class="btn btn-primary">Thêm hãng thời trang</button>
                      </a>
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Mã số</th>
                        <th>Tên hãng</th>
                        <th>Tác vụ</th>
                    

                      </tr>
                    </thead>
                    <tbody>
                        <?php
                        require '../inc/myconnect.php';
                        $query="select * from hangthoitrang";
                        $result = $conn->query($query); 
                        if ($result->num_rows > 0) {
                         while($row = $result->fetch_assoc()) {
                         ?>
                       <tr>
                        <td><?php echo $row['ma_hang']; ?></td>
                        <td><?php echo $row['ten_hang']; ?></td>
                        <td>
                            <a href="edithangthoitrang.php?mahang=<?php echo $row['ma_hang'] ?>"> <i title="Sửa" class="ion ion-edit"></i></a>
                            ------------
                            <a onclick="return confirm('Bạn có thật sự muốn xóa không ?');" href="xoahangthoitrang.php?ma_hang=<?php echo $row['ma_hang']; ?> "
                                 <i title="Xóa" class="ion-trash-a"></i></a>
                           
                        </td>
                       
                      </tr>
                            
                            
                        <?php
                         }
                        }
                        ?>
                     
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->
          </div><!-- /.row -->
          </div>
        </section><!-- /.content -->
        <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- page script -->
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
       <?php
       include 'footer.php';
       ?>
        </div>
    </body>
</html>
