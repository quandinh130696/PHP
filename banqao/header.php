<?php
require './xulydangnhap.php';
?>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Home | Quan-Shop's</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/prettyPhoto.css" rel="stylesheet">
        <link href="css/price-range.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
 
        
    </head>
    <body>
        <header id="header"><!--header-->
            <div class="header_top"><!--header_top-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="contactinfo">
                                <ul class="nav nav-pills">
                                    <li><a href="#"><i class="fa fa-phone"></i> +841639389160</a></li>
                                    <li><a href="#"><i class="fa fa-envelope"></i> quan.dinh130696@gmail.com</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="social-icons pull-right">
                           
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header_top-->

            <div class="header-middle"><!--header-middle-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="logo pull-left">
                                <a href="index.php"><img src="images/home/lg.png" style="width:350px;height:100px "/></a>
                                
                            </div>

                        </div>

                        <div class="col-sm-8">
                            <div class="shop-menu pull-right">
                                <ul class="nav navbar-nav">
                                    <li><a href="#"><i class="fa fa-user"></i> Account</a></li>
                                    <li><a href="cart.php"><i class="fa fa-shopping-cart"></i> Giỏ hàng <?php
                                    if (isset($_SESSION['cart'])){
                                            echo count($_SESSION['cart']) ;
                                    }
                                            ?></a></li>
                                    <li><a href="login.php"><i class="fa fa-lock"></i> Đăng nhập</a></li>
                                    <li><a href="admin/login.php"><i class="fa fa-lock"></i> Admin</a></li>
                                </ul>
                            </div>
               
                        </div>
                        <br/>
                        <br/>
                                     <div class="shop-menu pull-right">
                                <ul class="nav navbar-nav ">
                                    <li><?php
      
      if(isset($_SESSION['HoTen'])!="")
      {
          echo "<p> Xin chào: ".$_SESSION['HoTen']."<a href='xulylogout.php'>-----Logout</a></p>";
      }
      else
      {
          echo "<p> Bạn chưa đăng nhập </p>";
      }
    
 ?></li>
                            </ul>
                                
                            </div>
                    </div>
                </div>
            </div><!--/header-middle-->

            <div class="header-bottom"><!--header-bottom-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="mainmenu pull-left">
                                <ul class="nav navbar-nav collapse navbar-collapse">
                                    <li><a href="index.php" class="active">Trang chủ</a></li>
                                    <li class="dropdown"><a href="#">Shop<i class="fa fa-angle-down"></i></a>
                                        <ul role="menu" class="sub-menu">
                                            <li><a href="shop_1.php">Sản phẩm</a></li>

                                            <li><a href="cart.php">Giỏ hàng</a></li> 
                                            <li><a href="login.php">Đăng nhập</a></li> 
                                        </ul>
                                    </li> 


                                    <li><a href="contact.php">Liên hệ</a></li>
                                </ul>
                               
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="searchform">	
                                <form action="timkiem.php" class="searchform" method="GET">			
                                    <input type="text" placeholder="Search" name="txttimkiem" required="yes"/></a>	
                                    <button type="submit" name="timkiem" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-bottom-->
        </header><!--/header-->
        <section id="slider"><!--slider-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                                <li data-target="#slider-carousel" data-slide-to="1"></li>
                                <li data-target="#slider-carousel" data-slide-to="2"></li>
                            </ol>

                            <div class="carousel-inner">
                                <div class="item active">
                                    <div class="col-sm-6">
                                        <h1><span>Q</span>-SHOP</h1>
                                        <h2>Mẫu mã đa dạng</h2>
                                        <p>Thường xuyên cập nhật mẫu mã mới nhất.</p>

                                    </div>
                                    <div class="col-sm-6">
                                        <img style="width: 200px; height: 300px" src="images/home/nu.jpg" class="girl img-responsive" alt="" />
                                       
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-sm-6">
                                        <h1><span>Q</span>-SHOP</h1>
                                        <h2>Phong cách hiện đại</h2>
                                        <p>Các sản phẩm mang phong cách trẻ trung , hiện đại.</p>

                                    </div>
                                    <div class="col-sm-6">
                                        <img style="width: 200px; height: 300px"  src="images/home/maxresdefault.jpg" class="girl img-responsive" alt="" />
                                      
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="col-sm-6">
                                        <h1><span>Q</span>-SHOP</h1>
                                        <h2>Rẻ - Đẹp - Bền</h2>
                                        <p>Phù hợp với sinh viên.</p>

                                    </div>
                                    <div class="col-sm-6">
                                        <img style="width: 200px; height: 300px" src="images/home/sp.jpg" class="girl img-responsive" alt="" />
                                       
                                    </div>
                                </div>

                            </div>

                            <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </section><!--/slider-->
        <div class="col-sm-3">
            <div class="left-sidebar">

                <div class="brands_products"><!--brands_products-->
                    <h2>Thời trang phái</h2>
                    <div class="brands-name">
                        <ul class="nav nav-pills nav-stacked">                                             
                            <li><a href="thoitrangnam.php"> <span class="pull-right"></span>Thời trang nam</a></li>
                            <li><a href="thoitrangnu.php"> <span class="pull-right"></span>Thời trang nữ</a></li>

                        </ul>
                    </div>
                </div><!--/category-products-->

                <div class="brands_products"><!--brands_products-->
                    <h2>Hãng thời trang</h2>
                    <div class="brands-name">
                        <ul class="nav nav-pills nav-stacked">
                            <?php
                            require 'inc/truyvan.php';
                            if ($rs->num_rows > 0) {
                                // output data of each row
                                while ($row = $rs->fetch_assoc()) {
                                    ?>
                                    <li><a href="shop.php?mahang=<?php echo $row["ma_hang"] ?>"> <span class="pull-right"></span><?php echo $row["ten_hang"] ?></a></li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                </div><!--/brands_products-->



                <div class="shipping text-center"><!--shipping-->
                    <img src="images/home/shipping.jpg" alt="" />
                </div><!--/shipping-->
                <script src="js/jquery.js"></script>

            </div>
        </div>
    </body>
</html>
